package com.radu.network.tmdb.cache.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.radu.network.tmdb.cache.dao.MoviesDao
import com.radu.network.tmdb.cache.model.MovieCacheEntity
import java.io.File

@Database(entities = [MovieCacheEntity::class], version = 1)
abstract class MoviesDatabase : RoomDatabase() {

    abstract fun moviesDao(): MoviesDao

    companion object {
        private const val DB_NAME = "cache_db"

        fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                MoviesDatabase::class.java,
                context.cacheDir.absolutePath + File.separator + DB_NAME
            )
                .build()
    }
}