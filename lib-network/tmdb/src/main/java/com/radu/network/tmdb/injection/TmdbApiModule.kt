package com.radu.network.tmdb.injection

import android.content.Context
import com.radu.network.core.injection.NetworkCoreModule
import com.radu.network.tmdb.api.TmdbApiService
import com.radu.network.tmdb.cache.dao.MoviesDao
import com.radu.network.tmdb.cache.db.MoviesDatabase
import com.radu.network.tmdb.repository.TmdbApiRepository
import com.radu.network.tmdb.repository.TmdbApiRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module(includes = [NetworkCoreModule::class])
@InstallIn(SingletonComponent::class)
class TmdbApiModule {

    @Provides
    fun provideRetrofit(
        retrofitBuilder: Retrofit.Builder,
        @TmdbBaseUrl baseUrl: String
    ): Retrofit =
        retrofitBuilder
            .baseUrl(baseUrl)
            .build()

    @Provides
    fun provideTmdbApiService(retrofit: Retrofit): TmdbApiService =
        retrofit.create(TmdbApiService::class.java)

    @Provides
    fun provideTmdbApiRepository(repository: TmdbApiRepositoryImpl): TmdbApiRepository = repository

    @Provides
    fun provideMoviesDatabase(@ApplicationContext appContext: Context): MoviesDatabase {
        return MoviesDatabase.buildDatabase(appContext)
    }

    @Provides
    fun provideMoviesDao(db: MoviesDatabase): MoviesDao {
        return db.moviesDao()
    }
}