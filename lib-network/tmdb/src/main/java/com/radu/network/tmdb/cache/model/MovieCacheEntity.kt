package com.radu.network.tmdb.cache.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MovieCacheEntity(
    @PrimaryKey
    val id: Int,
    val title: String,
    val overview: String,
    val posterPath: String,
    val releaseDate: String,
    val average: String
)