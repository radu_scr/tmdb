package com.radu.network.tmdb.repository

import com.radu.network.tmdb.api.TmdbApiService
import com.radu.network.tmdb.injection.TmdbApiKey
import com.radu.network.tmdb.model.SortBy

import javax.inject.Inject

class TmdbApiRepositoryImpl @Inject constructor(
    private val apiService: TmdbApiService,
    @TmdbApiKey private val apiKey: String,
) : TmdbApiRepository {

    override suspend fun getPopularMovies(year: String, sortBy: SortBy) =
        apiService.getPopularMovies(
            apiKey = apiKey,
            releaseYear = year,
            sortBy = sortBy.type
        )
}