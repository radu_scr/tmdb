package com.radu.network.tmdb.cache.repository

import com.radu.network.tmdb.cache.dao.MoviesDao
import com.radu.network.tmdb.cache.model.MovieCacheEntity
import javax.inject.Inject

class MoviesCacheRepository @Inject constructor(
    private val moviesDao: MoviesDao
) {
    suspend fun insertMovie(movie: MovieCacheEntity) = moviesDao.insertMovie(movie)

    suspend fun insertMovies(movies: List<MovieCacheEntity>) = moviesDao.insertMovies(movies)

    suspend fun clearAllMovies() = moviesDao.clearAllMovies()

    suspend fun getAllMovies() = moviesDao.getAllMovies()

    suspend fun getMovie(id: Int) = moviesDao.getMovie(id)
}