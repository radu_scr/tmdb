package com.radu.network.tmdb.domain

import com.radu.network.core.cache.CacheStore
import com.radu.network.core.utils.ErrorWithCache
import com.radu.network.tmdb.cache.model.MovieCacheEntity
import com.radu.network.tmdb.cache.repository.MoviesCacheRepository
import com.radu.network.tmdb.domain.mapper.MovieMapper
import com.radu.network.tmdb.domain.model.Movie
import com.radu.network.tmdb.model.SortBy
import com.radu.network.tmdb.repository.TmdbApiRepository
import javax.inject.Inject

class GetPopularMovies @Inject constructor(
    private val repository: TmdbApiRepository,
    private val moviesCacheRepository: MoviesCacheRepository,
    private val mapper: MovieMapper
) : CacheStore<List<MovieCacheEntity>, List<Movie>> {

    suspend operator fun invoke(
        year: String,
        sortBy: SortBy
    ): Result<List<Movie>> {
        return try {
            repository.getPopularMovies(year, sortBy).fold(
                onSuccess = { response ->
                    clearCache()
                    saveToCache(response.results.map { mapper.remoteToCache(it) })
                    Result.success(retrieveFromCache())
                },
                onFailure = {
                    getMoviesFromCache(it)
                }
            )
        } catch (e: Exception) {
            getMoviesFromCache(e)
        }
    }

    private suspend fun getMoviesFromCache(throwable: Throwable): Result<List<Movie>> {
        val movies = retrieveFromCache()

        return if (movies.isNotEmpty()) {
            Result.failure(ErrorWithCache(movies))
        } else {
            Result.failure(throwable)
        }
    }

    override suspend fun clearCache() {
        moviesCacheRepository.clearAllMovies()
    }

    override suspend fun saveToCache(cache: List<MovieCacheEntity>) {
        moviesCacheRepository.insertMovies(cache)
    }

    override suspend fun retrieveFromCache(): List<Movie> =
        moviesCacheRepository.getAllMovies().map { mapper.cacheToModel(it) }
}