package com.radu.network.tmdb.injection

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class TmdbApiKey

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class TmdbBaseUrl

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class TmdbImagePath