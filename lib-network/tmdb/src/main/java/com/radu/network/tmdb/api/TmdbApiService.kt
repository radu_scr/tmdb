package com.radu.network.tmdb.api

import com.radu.network.tmdb.model.MoviesResponse
import retrofit2.http.*

interface TmdbApiService {

    @GET("discover/movie")
    suspend fun getPopularMovies(
        @Query("api_key") apiKey: String,
        @Query("primary_release_year") releaseYear: String,
        @Query("sort_by") sortBy: String
    ): Result<MoviesResponse>
}