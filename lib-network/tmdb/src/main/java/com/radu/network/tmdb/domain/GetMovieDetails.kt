package com.radu.network.tmdb.domain

import com.radu.network.core.cache.CacheStore
import com.radu.network.tmdb.cache.model.MovieCacheEntity
import com.radu.network.tmdb.cache.repository.MoviesCacheRepository
import com.radu.network.tmdb.domain.mapper.MovieMapper
import com.radu.network.tmdb.domain.model.Movie
import java.io.IOException
import javax.inject.Inject

class GetMovieDetails @Inject constructor(
    private val moviesCacheRepository: MoviesCacheRepository,
    private val movieMapper: MovieMapper
) : CacheStore<MovieCacheEntity, Movie?> {

    private var _movieId: Int = -1

    suspend operator fun invoke(movieId: Int): Result<Movie> {
        _movieId = movieId
        retrieveFromCache()?.let {
            return Result.success(it)
        } ?: return Result.failure(IOException())
    }

    override suspend fun clearCache() {}

    override suspend fun saveToCache(cache: MovieCacheEntity) {}

    override suspend fun retrieveFromCache(): Movie? {
        return moviesCacheRepository.getMovie(_movieId)?.let {
            movieMapper.cacheToModel(it)
        } ?: return null
    }
}