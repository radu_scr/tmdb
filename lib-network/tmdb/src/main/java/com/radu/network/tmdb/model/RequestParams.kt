package com.radu.network.tmdb.model

enum class SortBy(val type: String) {
    POPULARITY_ASC("popularity.asc"),
    POPULARITY_DESC("popularity.desc")
}