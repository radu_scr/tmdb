package com.radu.network.tmdb.domain.mapper

import com.radu.network.core.cache.RemoteCacheMapper
import com.radu.network.tmdb.cache.model.MovieCacheEntity
import com.radu.network.tmdb.domain.model.Movie
import com.radu.network.tmdb.model.MoviesResponse
import javax.inject.Inject

class MovieMapper @Inject constructor(
) : RemoteCacheMapper<MoviesResponse.MovieResult, MovieCacheEntity, Movie> {

    override fun remoteToCache(response: MoviesResponse.MovieResult): MovieCacheEntity =
        MovieCacheEntity(
            id = response.id,
            title = response.title,
            overview = response.overview,
            posterPath = response.posterPath,
            releaseDate = response.releaseDate,
            average = response.average
        )

    override fun cacheToModel(cache: MovieCacheEntity): Movie =
        Movie(
            id = cache.id,
            title = cache.title,
            overview = cache.overview,
            posterPath = cache.posterPath,
            releaseDate = cache.releaseDate,
            average = cache.average
        )
}