package com.radu.network.tmdb.repository

import com.radu.network.tmdb.model.MoviesResponse
import com.radu.network.tmdb.model.SortBy

interface TmdbApiRepository {

    suspend fun getPopularMovies(year: String, sortBy: SortBy): Result<MoviesResponse>
}