package com.radu.network.tmdb.cache.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.radu.network.tmdb.cache.model.MovieCacheEntity

@Dao
interface MoviesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovies(movies: List<MovieCacheEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovie(movie: MovieCacheEntity): Long

    @Query("SELECT * FROM moviecacheentity WHERE id = :id")
    suspend fun getMovie(id: Int): MovieCacheEntity?

    @Query("SELECT * FROM moviecacheentity")
    suspend fun getAllMovies(): List<MovieCacheEntity>

    @Query("DELETE FROM moviecacheentity WHERE id = :id")
    suspend fun deleteMovie(id: Int)

    @Query("DELETE FROM moviecacheentity")
    suspend fun clearAllMovies()
}