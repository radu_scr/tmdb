package com.radu.network.tmdb.repository

import com.radu.network.tmdb.api.TmdbApiService
import com.radu.network.tmdb.model.MoviesResponse
import com.radu.network.tmdb.model.SortBy
import com.radu.tmdb.testutils.BaseTest
import io.mockk.coEvery
import io.mockk.coVerify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class TmdbApiRepositoryImplTest : BaseTest() {

    companion object {
        private const val API_KEY = "apiKey"
    }

    private val mockApiService = mockk<TmdbApiService>()
    private val mockApiResult = mockk<MoviesResponse>()

    private lateinit var subject: TmdbApiRepositoryImpl

    @Before
    fun setUp() {
        subject = TmdbApiRepositoryImpl(mockApiService, API_KEY)
    }

    @Test
    fun `invoke api service`() = runTest {
        val year = "2022"
        val sortBy = SortBy.POPULARITY_DESC

        coEvery { mockApiService.getPopularMovies(API_KEY, year, sortBy.type) } returns
                Result.success(mockApiResult)

        subject.getPopularMovies(year, sortBy)

        coVerify(exactly = 1) {
            mockApiService.getPopularMovies(API_KEY, year, sortBy.type)
        }
    }
}