package com.radu.network.tmdb.domain

import com.radu.network.core.utils.ErrorWithCache
import com.radu.network.tmdb.cache.model.MovieCacheEntity
import com.radu.network.tmdb.cache.repository.MoviesCacheRepository
import com.radu.network.tmdb.domain.mapper.MovieMapper
import com.radu.network.tmdb.domain.model.Movie
import com.radu.network.tmdb.model.MoviesResponse
import com.radu.network.tmdb.model.SortBy
import com.radu.network.tmdb.repository.TmdbApiRepository
import com.radu.tmdb.testutils.BaseTest
import io.mockk.coEvery
import io.mockk.coVerify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class GetPopularMoviesTest : BaseTest() {

    private val mockApiRepository = mockk<TmdbApiRepository>()
    private val mockMoviesCacheRepository = mockk<MoviesCacheRepository>(relaxed = true)
    private val mockMapper = mockk<MovieMapper>()

    private val mockRemoteResponse = mockk<MoviesResponse>()
    private val mockMovieResult = mockk<MoviesResponse.MovieResult>()
    private val mockCachedMovie = mockk<MovieCacheEntity>()
    private val mockMovie = mockk<Movie>()

    private val year = "2022"
    private val sortBy = SortBy.POPULARITY_DESC

    private val listOfMovies = listOf(mockMovie)
    private val listOfCachedMovies = listOf(mockCachedMovie)

    private lateinit var subject: GetPopularMovies

    @Before
    fun setUp() {
        coEvery { mockMapper.remoteToCache(mockMovieResult) } returns mockCachedMovie
        coEvery { mockMapper.cacheToModel(mockCachedMovie) } returns mockMovie

        subject = GetPopularMovies(mockApiRepository, mockMoviesCacheRepository, mockMapper)
    }

    @Test
    fun `success with cache`() = runTest {
        coEvery { mockRemoteResponse.results } returns listOf(mockMovieResult)
        coEvery { mockApiRepository.getPopularMovies(year, sortBy) } returns Result.success(
            mockRemoteResponse
        )
        coEvery { mockMoviesCacheRepository.getAllMovies() } returns listOfCachedMovies

        val movies = subject.invoke(year, sortBy)

        coVerify(exactly = 1) {
            mockMoviesCacheRepository.clearAllMovies()
            mockMoviesCacheRepository.insertMovies(listOfCachedMovies)
        }

        assertThat(movies).isEqualTo(Result.success(listOfMovies))
    }

    @Test
    fun `network response unsuccessful - valid cache`() = runTest {
        coEvery { mockApiRepository.getPopularMovies(year, sortBy) } returns
                Result.failure(Throwable())
        coEvery { mockMoviesCacheRepository.getAllMovies() } returns listOfCachedMovies

        val movies = subject.invoke(year, sortBy)

        coVerify(exactly = 0) {
            mockMoviesCacheRepository.clearAllMovies()
            mockMoviesCacheRepository.insertMovies(listOfCachedMovies)
        }

        coVerify(exactly = 1) {
            mockMoviesCacheRepository.getAllMovies()
        }

        assertThat(movies).isEqualTo(Result.failure<List<Movie>>(ErrorWithCache(listOfMovies)))
    }
}