package com.radu.network.tmdb.domain.mapper

import com.radu.network.tmdb.cache.model.MovieCacheEntity
import com.radu.network.tmdb.domain.model.Movie
import com.radu.network.tmdb.model.MoviesResponse
import com.radu.tmdb.testutils.BaseTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class MovieMapperTest : BaseTest(){

    private val movieResponse = MoviesResponse.MovieResult(
        id = 1,
        title = "title",
        overview = "overview",
        posterPath = "posterPath",
        releaseDate = "releaseDate",
        average = "average"
    )

    private lateinit var subject: MovieMapper

    @Before
    fun setUp() {
        subject = MovieMapper()
    }

    @Test
    fun remoteToCache() {
        val result = subject.remoteToCache(movieResponse)

        assertThat(result).isEqualTo(
            MovieCacheEntity(
                movieResponse.id,
                movieResponse.title,
                movieResponse.overview,
                movieResponse.posterPath,
                movieResponse.releaseDate,
                movieResponse.average
            )
        )
    }

    @Test
    fun cacheToModel() {
        val result = subject.cacheToModel(subject.remoteToCache(movieResponse))

        assertThat(result).isEqualTo(
            Movie(
                movieResponse.id,
                movieResponse.title,
                movieResponse.overview,
                movieResponse.posterPath,
                movieResponse.releaseDate,
                movieResponse.average
            )
        )
    }
}