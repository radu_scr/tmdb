package com.radu.network.tmdb.domain

import com.radu.network.tmdb.cache.model.MovieCacheEntity
import com.radu.network.tmdb.cache.repository.MoviesCacheRepository
import com.radu.network.tmdb.domain.mapper.MovieMapper
import com.radu.network.tmdb.domain.model.Movie
import com.radu.tmdb.testutils.BaseTest
import io.mockk.coEvery
import io.mockk.coVerify
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class GetMovieDetailsTest : BaseTest() {

    companion object {
        private const val MOVIE_ID = 1234
    }

    private val mockMoviesCacheRepository = mockk<MoviesCacheRepository>()
    private val mockMovieMapper = mockk<MovieMapper>()
    private val mockMovieCache = mockk<MovieCacheEntity>()
    private val mockMovie = mockk<Movie>()

    private lateinit var subject: GetMovieDetails


    @Before
    fun setUp() {
        coEvery { mockMovieMapper.cacheToModel(mockMovieCache) } returns mockMovie

        subject = GetMovieDetails(
            mockMoviesCacheRepository,
            mockMovieMapper
        )
    }

    @Test
    fun `getMovie - success`() = runTest {
        coEvery { mockMoviesCacheRepository.getMovie(MOVIE_ID) } returns mockMovieCache

        val result = subject.invoke(MOVIE_ID)

        coVerify(exactly = 1) { mockMoviesCacheRepository.getMovie(MOVIE_ID) }
        assertThat(result).isEqualTo(Result.success(mockMovie))
    }

    @Test
    fun `getMovie - error`() = runTest {
        coEvery { mockMoviesCacheRepository.getMovie(MOVIE_ID) } returns null

        val result = subject.invoke(MOVIE_ID)

        coVerify(exactly = 1) { mockMoviesCacheRepository.getMovie(MOVIE_ID) }
        assertTrue(result.isFailure)
    }
}