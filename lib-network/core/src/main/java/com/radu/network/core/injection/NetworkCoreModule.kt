package com.radu.network.core.injection

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.radu.network.core.BuildConfig
import com.radu.network.core.utils.UnsafeOkHttpClient
import com.skydoves.retrofit.adapters.result.ResultCallAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
class NetworkCoreModule {

    @Provides
    fun provideJson(): Json = Json {
        explicitNulls = false
        ignoreUnknownKeys = true
        isLenient = true
        encodeDefaults = true
    }

    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        return if (BuildConfig.DEBUG) {
            UnsafeOkHttpClient.unsafeOkHttpClientBuilder
                .addInterceptor(
                    HttpLoggingInterceptor().apply {
                        level = HttpLoggingInterceptor.Level.BODY
                    }
                )
                .build()
        } else {
            OkHttpClient.Builder()
                .build()
        }
    }

    @Provides
    fun provideRetrofitBuilder(
        okHttpClient: OkHttpClient,
        json: Json
    ): Retrofit.Builder =
        Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(json.asConverterFactory(("application/json".toMediaType())))
            .addCallAdapterFactory(ResultCallAdapterFactory.create())
}