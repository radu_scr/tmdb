package com.radu.network.core.cache

interface RemoteCacheMapper<R, C, M> {

    fun remoteToCache(response: R): C

    fun cacheToModel(cache: C): M
}
