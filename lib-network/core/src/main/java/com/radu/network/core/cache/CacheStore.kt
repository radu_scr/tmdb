package com.radu.network.core.cache

interface CacheStore<C, M> {
    suspend fun clearCache()
    suspend fun saveToCache(cache: C)
    suspend fun retrieveFromCache(): M
}