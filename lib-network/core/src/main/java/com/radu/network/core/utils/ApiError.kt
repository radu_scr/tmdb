package com.radu.network.core.utils

data class ErrorWithCache(val cache: Any) : Throwable()
