package com.radu.tmdb.testutils

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.radu.tmdb.testutils.rules.CoroutineTestRule
import com.radu.tmdb.testutils.rules.MockKRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.rules.TestRule

abstract class ViewModelBaseTest {

    @get:Rule
    val mockKRule = MockKRule()

    @get:Rule
    val instantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    var coroutineTestRule = CoroutineTestRule()
}