package com.radu.tmdb.detail.viewmodel

import androidx.lifecycle.Observer
import com.radu.network.tmdb.domain.GetMovieDetails
import com.radu.network.tmdb.domain.model.Movie
import com.radu.tmdb.testutils.ViewModelBaseTest
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.verify
import org.junit.Before
import org.junit.Test

class MovieDetailViewModelTest : ViewModelBaseTest() {

    companion object {
        private const val MOVIE_ID = 1234
    }

    private val mockGetMovieDetails = mockk<GetMovieDetails>()
    private val stateObserver = mockk<Observer<MovieDetailViewModel.State>>(relaxed = true)
    private val mockMovie = mockk<Movie>()

    private lateinit var subject: MovieDetailViewModel

    @Before
    fun setUp() {
        subject = MovieDetailViewModel(mockGetMovieDetails).apply {
            state.observeForever(stateObserver)
        }
    }

    @Test
    fun `getMovie - success`() {
        coEvery { mockGetMovieDetails.invoke(MOVIE_ID) } returns Result.success(mockMovie)

        subject.getMovie(MOVIE_ID)

        verify(exactly = 1) {
            stateObserver.onChanged(MovieDetailViewModel.State.Loading)
        }
        coVerify(exactly = 1) {
            stateObserver.onChanged(MovieDetailViewModel.State.MovieLoaded(mockMovie))
        }
    }

    @Test
    fun `getMovie - error`() {
        coEvery { mockGetMovieDetails.invoke(MOVIE_ID) } returns Result.failure(Throwable())

        subject.getMovie(MOVIE_ID)

        verify(exactly = 1) {
            stateObserver.onChanged(MovieDetailViewModel.State.Loading)
        }
        coVerify(exactly = 1) {
            stateObserver.onChanged(MovieDetailViewModel.State.Error)
        }
    }
}