package com.radu.tmdb.detail.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.radu.network.tmdb.domain.GetMovieDetails
import com.radu.network.tmdb.domain.model.Movie
import com.radu.tmdb.base.core.viewmodel.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    private val getMovieDetails: GetMovieDetails
) : BaseViewModel() {

    sealed class State {
        object Loading : State()
        data class MovieLoaded(val movie: Movie) : State()
        object MovieNotSelected : State()
        object Error : State()
    }

    private var movieJob: Job? = null

    override val jobs = listOf(movieJob)

    val state: LiveData<State>
        get() = _state

    private val _state = MutableLiveData<State>()

    fun getMovie(movieId: Int) {
        if (movieId != 0) {
            _state.postValue(State.Loading)

            movieJob = CoroutineScope(Dispatchers.IO).launch {
                getMovieDetails(movieId)
                    .onSuccess {
                        withContext(Dispatchers.Main) {
                            _state.postValue(State.MovieLoaded(it))
                        }
                    }
                    .onFailure {
                        withContext(Dispatchers.Main) {
                            _state.postValue(State.Error)
                        }
                    }
            }
        } else {
            _state.postValue(State.MovieNotSelected)
        }
    }
}