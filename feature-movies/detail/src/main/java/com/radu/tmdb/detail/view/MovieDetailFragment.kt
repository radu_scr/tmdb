package com.radu.tmdb.detail.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.radu.network.tmdb.domain.model.Movie
import com.radu.network.tmdb.injection.TmdbImagePath
import com.radu.tmdb.base.core.extension.bindArgument
import com.radu.tmdb.base.core.extension.showSnackbar
import com.radu.tmdb.base.core.view.BaseFragment
import com.radu.tmdb.base.image.ImageLoader
import com.radu.tmdb.detail.R
import com.radu.tmdb.detail.databinding.FragmentMovieDetailBinding
import com.radu.tmdb.detail.viewmodel.MovieDetailViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MovieDetailFragment : BaseFragment<FragmentMovieDetailBinding>() {

    companion object {
        const val ARG_MOVIE_ID = "movie_id"
    }

    @Inject
    lateinit var imageLoader: ImageLoader

    @Inject
    @TmdbImagePath
    lateinit var imagePath: String

    private val viewModel: MovieDetailViewModel by viewModels()

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentMovieDetailBinding
        get() = FragmentMovieDetailBinding::inflate

    private val movieId by bindArgument<Int>(ARG_MOVIE_ID)

    override fun initWidgets(view: View) {
        super.initWidgets(view)
        getMovie()
    }

    override fun observeViewModels() {
        super.observeViewModels()
        viewModel.state.observe(this) { state ->
            when (state) {
                is MovieDetailViewModel.State.Loading -> {
                    showLoading()
                }
                is MovieDetailViewModel.State.MovieLoaded -> {
                    hideLoading()
                    populateMovieDetails(state.movie)
                }
                is MovieDetailViewModel.State.MovieNotSelected -> {
                    hideLoading()
                    onNothingSelected()
                }
                is MovieDetailViewModel.State.Error -> {
                    hideLoading()
                    binding.itemDetailContainer.showSnackbar(R.string.error, R.string.try_again) {
                        getMovie()
                    }
                }
            }
        }
    }

    private fun getMovie() {
        viewModel.getMovie(movieId)
    }

    private fun showLoading() {
        with(binding) {
            loadingProgress.visibility = View.VISIBLE
            appBar.visibility = View.GONE
            itemDetailScrollView.visibility = View.GONE
            nothingSelected?.visibility = View.GONE
        }
    }

    private fun hideLoading() {
        with(binding) {
            loadingProgress.visibility = View.GONE
            appBar.visibility = View.VISIBLE
            itemDetailScrollView.visibility = View.VISIBLE
        }
    }

    private fun onNothingSelected() {
        with(binding) {
            nothingSelected?.visibility = View.VISIBLE
            appBar.visibility = View.GONE
            itemDetailScrollView.visibility = View.GONE
        }
    }

    private fun populateMovieDetails(movie: Movie) {
        with(binding) {
            nothingSelected?.visibility = View.GONE
            toolbarLayout.title = movie.title
            itemDetail.text = movie.overview
            imageLoader.loadImageOriginalSize("$imagePath${movie.posterPath}", moviePoster)
        }
    }
}