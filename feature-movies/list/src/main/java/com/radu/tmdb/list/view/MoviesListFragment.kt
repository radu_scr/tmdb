package com.radu.tmdb.list.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.radu.network.tmdb.cache.model.MovieCacheEntity
import com.radu.network.tmdb.domain.model.Movie
import com.radu.network.tmdb.model.SortBy
import com.radu.tmdb.base.core.extension.showSnackbar
import com.radu.tmdb.base.core.view.BaseFragment
import com.radu.tmdb.detail.view.MovieDetailFragment
import com.radu.tmdb.list.R
import com.radu.tmdb.list.adapter.MoviesListAdapter
import com.radu.tmdb.list.databinding.FragmentMovieListBinding
import com.radu.tmdb.list.viewmodel.MoviesListViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class MoviesListFragment : BaseFragment<FragmentMovieListBinding>() {

    @Inject
    lateinit var moviesAdapter: MoviesListAdapter

    private val viewModel: MoviesListViewModel by viewModels()

    private var itemDetailFragmentContainer: View? = null

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentMovieListBinding
        get() = FragmentMovieListBinding::inflate

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getMovies()
    }

    override fun initWidgets(view: View) {
        super.initWidgets(view)
        // Leaving this not using view binding as it relies on if the view is visible the current
        // layout configuration (layout, layout-sw600dp)
        itemDetailFragmentContainer = view.findViewById(R.id.item_detail_nav_container)

        binding.itemList.adapter = moviesAdapter
    }

    override fun observeViewModels() {
        super.observeViewModels()
        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                MoviesListViewModel.State.Loading -> {
                    showLoading()
                }
                MoviesListViewModel.State.Error -> {
                    hideLoading()
                    binding.itemListContainer.showSnackbar(R.string.error, R.string.try_again) {
                        getMovies()
                    }
                }
                is MoviesListViewModel.State.Movies -> {
                    hideLoading()
                    populateMoviesList(state.movies)
                    if (state.fromCache) {
                        binding.itemListContainer.showSnackbar(
                            R.string.error_cache,
                            R.string.try_again
                        ) {
                            getMovies()
                        }
                    }
                }
            }
        }
    }

    private fun getMovies() {
        viewModel.getMovies(
            year = "${Calendar.getInstance().get(Calendar.YEAR)}",
            sortBy = SortBy.POPULARITY_DESC
        )
    }

    private fun showLoading() {
        with(binding) {
            loadingProgress.visibility = View.VISIBLE
            contentGroup?.visibility = View.GONE
            itemList.visibility = View.GONE
        }
    }

    private fun hideLoading() {
        with(binding) {
            loadingProgress.visibility = View.GONE
            contentGroup?.visibility = View.VISIBLE
            itemList.visibility = View.VISIBLE
        }
    }

    private fun populateMoviesList(movies: List<Movie>) {
        moviesAdapter.populate(movies) { movieId ->
            val bundle = Bundle().apply {
                putInt(MovieDetailFragment.ARG_MOVIE_ID, movieId)
            }

            // Trigger navigation based on if you have a single pane layout or two pane layout
            itemDetailFragmentContainer?.findNavController()
                ?.navigate(R.id.fragment_movie_detail, bundle)
                ?: binding.itemList.findNavController().navigate(R.id.show_movie_details, bundle)
        }
    }
}