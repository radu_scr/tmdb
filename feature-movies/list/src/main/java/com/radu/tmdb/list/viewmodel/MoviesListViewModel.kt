package com.radu.tmdb.list.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.radu.network.core.utils.ErrorWithCache
import com.radu.network.tmdb.domain.GetPopularMovies
import com.radu.network.tmdb.domain.model.Movie
import com.radu.network.tmdb.model.SortBy
import com.radu.tmdb.base.core.viewmodel.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

@HiltViewModel
class MoviesListViewModel @Inject constructor(
    private val getPopularMovies: GetPopularMovies
) : BaseViewModel() {

    sealed class State {
        object Loading : State()
        data class Movies(val movies: List<Movie>, val fromCache: Boolean) : State()
        object Error : State()
    }

    private var moviesJob: Job? = null

    override val jobs = listOf(moviesJob)

    private val exceptionHandler =
        CoroutineExceptionHandler { _: CoroutineContext, _: Throwable ->
            _state.postValue(State.Error)
        }

    val state: LiveData<State>
        get() = _state

    private val _state = MutableLiveData<State>()

    fun getMovies(year: String, sortBy: SortBy) {
        _state.postValue(State.Loading)

        moviesJob = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            getPopularMovies(year, sortBy)
                .onSuccess {
                    withContext(Dispatchers.Main) {
                        _state.postValue(State.Movies(it, fromCache = false))
                    }
                }
                .onFailure {
                    when (it) {
                        is ErrorWithCache -> {
                            withContext(Dispatchers.Main) {
                                _state.postValue(
                                    State.Movies(
                                        it.cache as List<Movie>,
                                        fromCache = true
                                    )
                                )
                            }
                        }
                        else -> {
                            withContext(Dispatchers.Main) {
                                _state.postValue(State.Error)
                            }
                        }
                    }
                }
        }
    }
}