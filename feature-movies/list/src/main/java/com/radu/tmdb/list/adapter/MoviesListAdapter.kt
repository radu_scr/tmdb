package com.radu.tmdb.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.radu.network.tmdb.cache.model.MovieCacheEntity
import com.radu.network.tmdb.domain.model.Movie
import com.radu.network.tmdb.injection.TmdbImagePath
import com.radu.tmdb.base.image.ImageLoader
import com.radu.tmdb.list.databinding.ItemMovieBinding
import javax.inject.Inject

class MoviesListAdapter @Inject constructor(
    @TmdbImagePath private val imagePath: String,
    private val imageLoader: ImageLoader
) : RecyclerView.Adapter<MoviesListAdapter.ViewHolder>() {

    private val movies = mutableListOf<Movie>()
    private var onItemCLick: ((Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemMovieBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = movies[position]

        imageLoader.loadImage(
            imageUrl = "$imagePath${item.posterPath}",
            target = holder.image
        )

        with(holder) {
            title.text = item.title
            average.text = item.average
            releaseDate.text = item.releaseDate

            itemView.setOnClickListener {
                onItemCLick?.invoke(item.id)
            }
        }
    }

    override fun getItemCount() = movies.size

    fun populate(listOfMovies: List<Movie>, onCLick: (itemId: Int) -> Unit) {
        with(movies) {
            clear()
            addAll(listOfMovies)
        }
        this.onItemCLick = onCLick

        notifyDataSetChanged()
    }


    inner class ViewHolder(binding: ItemMovieBinding) : RecyclerView.ViewHolder(binding.root) {
        val image: ImageView = binding.image
        val title: TextView = binding.title
        val average: TextView = binding.average
        val releaseDate: TextView = binding.releaseDate
    }
}