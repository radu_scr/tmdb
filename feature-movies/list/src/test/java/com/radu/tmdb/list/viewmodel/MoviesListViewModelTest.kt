package com.radu.tmdb.list.viewmodel

import androidx.lifecycle.Observer
import com.radu.network.core.utils.ErrorWithCache
import com.radu.network.tmdb.domain.GetPopularMovies
import com.radu.network.tmdb.domain.model.Movie
import com.radu.network.tmdb.model.SortBy
import com.radu.tmdb.testutils.ViewModelBaseTest
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.verify
import org.junit.Before
import org.junit.Test

class MoviesListViewModelTest : ViewModelBaseTest(){

    private val mockGetPopularMovies = mockk<GetPopularMovies>()
    private val stateObserver = mockk<Observer<MoviesListViewModel.State>>(relaxed = true)
    private val mockMovies = mockk<List<Movie>>()


    private val year = "2022"
    private val sortBy = SortBy.POPULARITY_DESC

    private lateinit var subject: MoviesListViewModel

    @Before
    fun setUp() {
        subject = MoviesListViewModel(mockGetPopularMovies).apply {
            state.observeForever(stateObserver)
        }
    }

    @Test
    fun `getMovies - success`() {
        coEvery { mockGetPopularMovies.invoke(year, sortBy) } returns Result.success(mockMovies)

        subject.getMovies(year, sortBy)

        verify(exactly = 1) {
            stateObserver.onChanged(MoviesListViewModel.State.Loading)
        }
        coVerify(exactly = 1) {
            stateObserver.onChanged(MoviesListViewModel.State.Movies(mockMovies, false))
        }
    }

    @Test
    fun `getMovies - success from cache`() {
        coEvery { mockGetPopularMovies.invoke(year, sortBy) } returns Result.failure(ErrorWithCache(mockMovies))

        subject.getMovies(year, sortBy)

        verify(exactly = 1) {
            stateObserver.onChanged(MoviesListViewModel.State.Loading)
        }
        coVerify(exactly = 1) {
            stateObserver.onChanged(MoviesListViewModel.State.Movies(mockMovies, true))
        }
    }

    @Test
    fun `getMovies - error`() {
        coEvery { mockGetPopularMovies.invoke(year, sortBy) } returns Result.failure(Exception())

        subject.getMovies(year, sortBy)

        verify(exactly = 1) {
            stateObserver.onChanged(MoviesListViewModel.State.Loading)
        }
        coVerify(exactly = 1) {
            stateObserver.onChanged(MoviesListViewModel.State.Error)
        }
    }
}