package com.radu.tmdb.injection

import com.radu.network.tmdb.injection.TmdbApiKey
import com.radu.network.tmdb.injection.TmdbBaseUrl
import com.radu.network.tmdb.injection.TmdbImagePath
import com.radu.tmdb.BuildConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object UrlModule {

    @Provides
    @TmdbBaseUrl
    fun provideTmdbBaseUrl(): String = BuildConfig.TMDB_API_PATH

    @Provides
    @TmdbImagePath
    fun provideTmdbImagePath(): String = BuildConfig.TMDB_IMAGE_PATH

    @Provides
    @TmdbApiKey
    fun provideTmdbApiKey(): String = BuildConfig.TMDB_KEY
}