package com.radu.tmdb.injection

import com.radu.tmdb.base.image.GlideImageLoader
import com.radu.tmdb.base.image.ImageLoader
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module(includes = [UrlModule::class])
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    fun provideImageLoader(loader: GlideImageLoader): ImageLoader = loader
}