package com.radu.tmdb.view

import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isRoot
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.radu.tmdb.injection.UrlModule
import com.radu.tmdb.list.R
import com.radu.tmdb.waitFor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import dagger.hilt.components.SingletonComponent
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.File

@HiltAndroidTest
@UninstallModules(UrlModule::class)
class HomeActivityTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    lateinit var mainScenario: ActivityScenario<HomeActivity>

    @Module
    @InstallIn(SingletonComponent::class)
    class FakeBaseUrlModule {

        @Provides
        fun provideTmdbBaseUrl(): String = "http://localhost:8080/"
    }

    private lateinit var mockWebServer: MockWebServer

    @Before
    fun setup() {
        mockWebServer = MockWebServer()
        mockWebServer.start(8080)

        hiltRule.inject()

    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun happyTestCase() {
        setNetworkResponse("movies_success.json")

        mainScenario = ActivityScenario.launch(HomeActivity::class.java)

        tapMovieInList()
        verifyMovieDetails()
    }

    private fun setNetworkResponse(responsePath: String) {
        mockWebServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return MockResponse()
                    .setResponseCode(200)
                    .setBody(File(responsePath).readText(Charsets.UTF_8))
            }
        }
    }

    private fun tapMovieInList() {
        onView(isRoot()).perform(waitFor(500))

        onView(withId(R.id.item_list))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click())
            )
    }

    private fun verifyMovieDetails() {
        onView(isRoot()).perform(waitFor(500))

        onView(withId(R.id.movie_poster)).run {
            check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        }
    }
}