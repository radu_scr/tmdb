package com.radu.tmdb.base.core.viewmodel

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Job

abstract class BaseViewModel : ViewModel() {

    abstract val jobs: List<Job?>

    override fun onCleared() {
        super.onCleared()
        jobs.forEach {
            it?.cancel()
        }
    }
}