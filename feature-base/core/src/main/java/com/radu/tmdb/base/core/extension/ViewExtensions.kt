package com.radu.tmdb.base.core.extension

import android.view.View
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar

inline fun <reified T : Any> Fragment.bindArgument(key: String): Lazy<T> = lazy {
    arguments?.get(key) as? T ?: throw IllegalArgumentException("Argument not passed for key: $key")
}

fun View.showSnackbar(
    @StringRes message: Int,
    @StringRes action: Int,
    @BaseTransientBottomBar.Duration duration: Int = Snackbar.LENGTH_INDEFINITE,
    onActionClick: () -> Unit
) {
    showSnackbar(context.getString(message), context.getString(action), duration, onActionClick)
}

fun View.showSnackbar(
    message: String,
    action: String,
    @BaseTransientBottomBar.Duration duration: Int = Snackbar.LENGTH_INDEFINITE,
    onActionClick: () -> Unit
) {
    with(Snackbar.make(this, message, duration)) {
        setAction(action) {
            this.dismiss()
            onActionClick()
        }
        show()
    }
}