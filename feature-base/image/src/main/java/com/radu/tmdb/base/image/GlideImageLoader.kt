package com.radu.tmdb.base.image

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class GlideImageLoader @Inject constructor(
    @ApplicationContext private val context: Context
) : ImageLoader {

    override fun loadImage(imageUrl: String, target: ImageView) {
        Glide
            .with(context)
            .load(imageUrl)
            .centerInside()
            .into(target)
    }

    override fun loadImageOriginalSize(imageUrl: String, target: ImageView) {
        Glide
            .with(context)
            .load(imageUrl)
            .into(target)
    }
}