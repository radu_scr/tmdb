package com.radu.tmdb.base.image

import android.widget.ImageView

interface ImageLoader {
    fun loadImage(imageUrl: String, target: ImageView)
    fun loadImageOriginalSize(imageUrl: String, target: ImageView)
}